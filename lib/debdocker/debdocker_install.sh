#!/bin/bash
#
# The "debdocker_install-deps.sh" in-container executable
#
# This file is part of the "debdocker" software project.
#
#  Copyright (C) 2020 Samo Pogacnik <samo_pogacnik@t-2.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

#set -x
#set -e
#exit 99

DEBS=""
DEBPKGS=""
while [[ $# > 0 ]]; do
	if [ -f "${1}" ]; then
		DEBPKGS="${DEBPKGS} $(echo -n ${1} | sed 's/_[0-9].*//')"
		DEBS="${DEBS} $(echo -n "/${1}")"
	else
		echo "ERROR - Missing file '${1}'!"
		exit 1
	fi
	shift
done

DEBPKGS=$(echo $DEBPKGS | sed 's/ *//')
DEBS=$(echo $DEBS | sed 's/ *//')
#SKIPCOMMIT=0
echo
echo "Removing packages '${DEBPKGS}'!"
#set -x
apt-get purge -y --simulate ${DEBPKGS}
DEBSTAT="$(apt-get purge -y ${DEBPKGS} 2>&1 | sed 's/E: Unable to locate package/ 0 to remove /' | grep ' 0 to remove ')"
#set +x
if [ -z "$DEBSTAT" ]; then
	echo "Previously installed packages removed!"
#	SKIPCOMMIT=1
else
	echo "Initially installing packages!"
fi

echo
echo "Installing dependencies for '${DEBS}'!"
#set -x
DEPENDS="$( \
	apt-get -y --no-install-recommends --simulate install ${DEBS} | \
	tr '\n' ' ' | \
	sed 's/.*The following additional packages will be installed://' | \
	sed 's/Suggested packages:.*//' | \
	sed 's/^Reading package lists.*//')"
#set +x
echo "Dependencies: ${DEPENDS}"
apt-get -y --no-install-recommends install ${DEPENDS}
rv=$?
if [ $rv -ne 0 ]; then
	echo "Failed to install dependencies for '${DEBS}'!"
	exit $rv
fi

echo
echo "Installing '${DEBS}'!"
dpkg -i ${DEBS}
exit $?
