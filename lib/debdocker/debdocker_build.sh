#!/bin/bash
#
# The "debdocker_build.sh" in-container executable
#
# This file is part of the "debdocker" software project.
#
#  Copyright (C) 2020 Samo Pogacnik <samo_pogacnik@t-2.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

#set -x
#set -e

PKGDIR="${1}"
shift
DSCBASE="${1}"
DEBUILD="${@}"

if [ -n "${DSCBASE}" ]; then
	if [ -e "${DSCBASE}.dsc" ]; then
		shift
		DEBUILD="${@}"
	else
		DSCBASE=""
	fi
fi

if [ -z "${DSCBASE}" ]; then
	echo "Use existing 'package_dir'!"
else
	if [ "${DSCBASE}" = "$(basename ${PKGDIR} .build)" ]; then
		echo "Remove existing and Create new 'package_dir' ('dsc_file' and 'package_dir' base names match)!"
		rm -rf $PKGDIR
		cmd="dpkg-source -x ${DSCBASE}.dsc ${PKGDIR}"
		eval $cmd
		rv=$?
		if [ $rv -ne 0 ]; then
			echo "Failure (${rv}) - ${cmd}"
			exit $rv
		fi
	else
		echo "Use existing 'package_dir'!"
	fi
fi
cmd="cd ${PKGDIR}"
eval $cmd
rv=$?
if [ $rv -ne 0 ]; then
	echo "Failure (${rv}) - ${cmd}"
	exit $rv
fi

cmd="debuild ${DEBUILD}"
echo "Running - ${cmd}"
eval $cmd
rv=$?
if [ $rv -ne 0 ]; then
	echo "Failure (${rv}) - ${cmd}"
else
	echo "Success - ${cmd}"
fi
exit $rv
