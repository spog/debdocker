#!/bin/bash
#
# The "debdocker_build-deps.sh" in-container executable
#
# This file is part of the "debdocker" software project.
#
#  Copyright (C) 2020 Samo Pogacnik <samo_pogacnik@t-2.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

#set -x
#set -e
#exit 99

SKIPCOMMIT=0
DEPSSTAT="$(dpkg -P pbuilder-satisfydepends-dummy 2>&1 | grep "ignoring request to remove pbuilder-satisfydepends-dummy")"
if [ -z "$DEPSSTAT" ]; then
	echo "Previous ${1} build dependencies already met!"
	SKIPCOMMIT=1
else
	echo "Initially resolving build dependencies for ${1}!"
fi

/usr/lib/pbuilder/pbuilder-satisfydepends-aptitude --control /${1} | tee /tmp/pbuilder-satisfydepends-dummy.build_deps
if [ -n "$(grep "No packages will be installed, upgraded, or removed." /tmp/pbuilder-satisfydepends-dummy.build_deps)" ]; then
	if [ ${SKIPCOMMIT} -ne 0 ]; then
		echo "Current ${1} build dependencies already met - skip container commit!"
		exit 100
	fi
fi
exit $?
