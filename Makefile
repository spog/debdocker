export PREFIX := /usr

all:

install:
	install -d -m 0755 $(DESTDIR)$(PREFIX)/bin
	install -C -m 0755 debdocker $(DESTDIR)$(PREFIX)/bin/
	install -d -m 0755 $(DESTDIR)$(PREFIX)/lib/debdocker
	install -C -m 0644 lib/debdocker/* $(DESTDIR)$(PREFIX)/lib/debdocker/
	chmod 755 $(DESTDIR)$(PREFIX)/lib/debdocker/*sh
	install -d -m 0755 $(DESTDIR)$(PREFIX)/share/debdocker
	install -C -m 0644 share/debdocker/* $(DESTDIR)$(PREFIX)/share/debdocker/
	chmod 440 $(DESTDIR)$(PREFIX)/share/debdocker/debdocker.sudoers
	install -d -m 0755 $(DESTDIR)$(PREFIX)/share/man/man8
	gzip -c share/man/man8/debdocker.8 > $(DESTDIR)$(PREFIX)/share/man/man8/debdocker.8.gz
